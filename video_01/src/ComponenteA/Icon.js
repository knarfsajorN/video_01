import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    width: 25%;
`
const Body = styled.div`
    background-color: ${props => props.colorIcon};
    height:100%;
    width:100%;
    border-radius:25px;
    display:flex;
    justify-content:center;
    align-items:center;
`
const Icon = styled.img`
    height:45px;
    widht:45px;
    filter: ${props=> props.colorLine}
` 

const App = ({icon,colorIcon,colorLine}) => {
    console.log(colorLine);
    return(
        <Container>
            <Body colorIcon={colorIcon} >
                <Icon src={icon} colorLine={colorLine}/>
            </Body>
            
        </Container>
    );
}

export default App;