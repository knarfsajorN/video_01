import React from 'react';
import styled from 'styled-components';
import ComponentA from './ComponenteA';
import ComponentB from './ComponenteB';
import buy from './Images/buy.png';
import analitic from './Images/analitic.png';
import shake from './Images/shake_hand.png';
import percent from './Images/percent.png';
import computer from './Images/computer.png';


import king from './Images/king.png';
import search from './Images/search.png';
import message from './Images/message.png';

import right from './Images/bbb.png';

const Container = styled.div`
  height: 40%;
  width : 65%;
  display:flex;
  //flex-direction:row;
  //align-items:center;
  // margin-top:15px;
  // border:1px solid black;
  // border-radius:5px;
`
const Container1 = styled.div`
  height: 100%
  width : 100%;
  
  display:flex;
  flex-direction:column;
  align-items:center;
  margin-top:15px;
  // border:1px solid black;
  border-radius:5px;
`

const Container2 = styled.div`
  height: 100%
  width : 100%;
  padding-top:0px;
  display:flex;
  flex-direction:column;
  //align-items:center;
  //margin-top:15px;
  // border:1px solid black;
  border-radius:5px;
`

// const Container3 = styled.div`
//   height: 100%
//   width : 100%;
//   padding-top:0px;
//   display:flex;
//   flex-direction:column;
//   //align-items:center;
//   //margin-top:15px;
//   // border:1px solid black;
//   border-radius:5px;
// `

const App =() => {
  return (
    <Container >

      <Container1>
        <ComponentB text={"Pedidos"} icon={buy} colorIcon={"#fff6e5"} icon2={right} bold={"true"} visible={3}/>
        <ComponentB text={"Analitica de negocio"} icon={analitic} colorIcon={"#ebfcf1"} icon2={right} bold={""}/>
        <ComponentB text={"Clientes"} icon={shake} colorIcon={"#f4f2ff"} icon2={right}/>
        <ComponentB text={"Descuentos"} icon={percent} colorIcon={"#fff6e5"} icon2={right}/>
        <ComponentB text={"Formularios recibidos"} icon={computer} colorIcon={"#ebfcf1"} icon2={right} />
      </Container1>
      
      <Container2>
      <ComponentA icon={king} colorIcon={"#fff6e5"} colorLine={"opacity(0.1) drop-shadow(0 0 0 #fff6e5)"}/>
      <ComponentA icon={search} colorIcon={"#ebfcf1"} colorLine={"opacity(0.1) drop-shadow(0 0 0 #ebfcf1)"}/>
      <ComponentA icon={message} colorIcon={"#f4f2ff"} colorLine={"opacity(0.1) drop-shadow(0 0 0 #f4f2ff)"}/>
      </Container2>
      
        
      
    </Container>
    
  );
}

export default App;

