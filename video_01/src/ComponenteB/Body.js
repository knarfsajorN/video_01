import React from "react";
import styled from "styled-components";


const Container = styled.div`
    height: 90%;
    width: 100%;
    padding-left:5px;
    display:flex;
    flex-direction:row;
    justify-content:left;
    align-items: center;
    

`
const Title = styled.p`
    margin:0px;
    color:#657290;
    font-weight:${props => props.bold ? 'bold' : ''};
    font-size:15px;
`

export const Circle = styled.div`
    border-radius:50%;
    border:1.8px solid white;
    height:15px;
    width:15px;
    cursor:pointer;
    margin:2px;
    //background-color:red;
    background-color: ${props => props.visible > 0 ? '#cb1010':'transparent'};
`

const Notification = styled.p`
    //margin:0px;
    margin-top:0px;
    color:white;
    font-size:10px;
    align-items:center;
    display:flex;
    justify-content:center;
`
// const Enlace = styled.a`
//     margin:0px;
//     href:'www.gmail.com';
// `

const App = ({text,bold,visible}) => {
    return(
        <Container>
            <Title bold={bold}>
                {text}
            </Title>
            <Circle visible={visible}>
                <Notification>{visible}</Notification>
            </Circle>
            {/* <Descripcion>
                Descubre nuevas funciones
            </Descripcion>
            <Enlace>
                Enlace
            </Enlace> */}
        </Container>
    );
}

export default App;