import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    width: 5%;
`
const Body = styled.div`
    //background-color: ${props => props.colorIcon};
    height:75%;
    width:100%;
    //border-radius:25px;
    //display:flex;
    //justify-content:right;
    align-items:right;
    border:1;
`
const Icon = styled.img`
    height:30px;
    filter: opacity(0.2) drop-shadow(0 0 0 	#87CEEB);
    // align-items:right;
    //filter:
` 

const App = ({icon,colorIcon}) => {
    console.log(colorIcon);
    return(
        <Container>
            <Body colorIcon={colorIcon} >
                <Icon src={icon}/>
            </Body>
            
        </Container>
    );
}

export default App;