import React from "react";
import styled from "styled-components";

import Icon from './Icon';
import Body from './Body';
import Icon2 from './Icon2';

const Container = styled.div`
    height: 70px;
    width : 800px;
    display:flex;
    flex-direction:row;
    align-items:center;
    // margin-top:10px;
    border:3px solid #F5F5F5;
    //border-collapse: collapse;
    //border-color:"#FFFFFF" ;
    border-radius:5px;
    margin-left:65px;
    margin-right:65px;
`

const App = (params) => {
    return(
        <Container >
            <Icon icon={params.icon} colorIcon={params.colorIcon} ></Icon>
            <Body text={params.text} bold={params.bold} visible={params.visible}></Body>
            <Icon2 icon={params.icon2} colorIcon={params.colorIcon} ></Icon2>
        </Container>
    );
}


export default App;