import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    width: 20%;
`
const Body = styled.div`
    //background-color: ${props => props.colorIcon};
    height:75%;
    width:100%;
    //border-radius:25px;
    display:flex;
    justify-content:center;
    align-items:center;
    border:1;
    
`
const Icon = styled.img`
    height:30px;
    //color:red; 
    //color:"#87CEEB"
    filter: opacity(0.2) drop-shadow(0 0 0 	#00aae4);
    //filter: ${props => props.visible ? 'opacity(0.2) drop-shadow(0 0 0 blue)':'transparent'};
    //background: linear-gradient(purple, violet);
` 

const App = ({icon,colorIcon}) => {
    console.log(colorIcon);
    return(
        <Container>
            <Body colorIcon={colorIcon} >
                <Icon  src={icon}/>
            </Body>
            
        </Container>
    );
}

export default App;